# coding: utf-8
import csv
import os
import pdfkit


# Remplace l'UAI par le nom de l'établissement avec la ville
def nom_etab(uai):
    with open('./stats/annuaire_etab.csv', encoding='utf-8', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='|')
        for row in reader:
            if row[0]==uai:
                etab=(row[1]+', '+row[2])
        csvfile.close()
    return(etab)

# Remplace l'ark par le nom de la ressource, simplifier le nom s'il est utilisé comme nom de fichier
def nom_ressource(ark,simplifie):
    with open('./stats/Ressources_tne.csv', encoding='utf-8', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='|')
        for row in reader:
            row[0] = row[0].replace(":", "")
            row[0] = row[0].replace("/", "")
            if row[0]==ark:
                intitule=row[1]
        csvfile.close()
        intitule=intitule.replace("(TNE)","")
        if simplifie :
            intitule=intitule.replace(" ","")
            intitule=intitule.replace("'","")
            intitule=intitule.replace("#","")
    return(intitule)

    for grain in Ressources_TNE:
        if grain[0]==ark:
            return (grain[1])

#Ecris toutes les données d'une ressource dans un fichier
def html_ressource(ark,debut,fin):
    #charge les cumuls par établissements correspondants à la ressource
    donnees_etablissements=[]
    with open('./data/bilan_'+ark+'.csv', encoding='utf-8', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='"')
        for row in reader:
            row[1]=int(row[1])
            donnees_etablissements.append(row)
        csvfile.close()

    #Trie par odre décroissant de connexions élèves
    donnees_etablissements=(sorted(donnees_etablissements, key=lambda x: x[1], reverse=True))

    with open('./exports/'+ark+'.html', "w", encoding='utf-8') as fichier:
        fichier.write("<!doctype html>\n<html>\n<head>\n<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>\n<link href=\"../style.css\" rel=\"stylesheet\" type=\"text/css\" />\n<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\"><link href=\"https://fonts.googleapis.com/css2?family=Acme&family=Open+Sans&family=Roboto&display=swap\" rel=\"stylesheet\">\n<body>\n")
        fichier.write("<div class='titre new-page' id='"+ark+"'><div class='home'><a href='bilan_ressources.pdf'>Retour</a></div><h1>Statistiques d'utilisation du GAR en Isère<br>Ressource "+str(nom_ressource(ark,False))+"</h1>\n")
        fichier.write("<h3>Période du "+debut+" au "+fin+"<br>"+"</h3>\n</div>")
        fichier.write("<h2>Nombre de connexions sur l\'année</h2>\n")
        fichier.write("<img src='../data/img/"+ark+"_connexions.png' width=100% align='center'\>")
        
        fichier.write("<h2>Établissements qui utilisent cette ressource</h2>\n<p style='font-size: x-small;'>Par ordre décroissant de connexion élèves<br>connexions cumulées sur l'année, ne représente pas le nombre d'utilisateurs distincts</p><table><tr class='descripteurs'><th>Nom de l'établissements</th><th>connexions<br>élè / ens</th><th>Licences<br>élè / ens</th></tr>")
        for etablissement in donnees_etablissements:
            fichier.write("<tr><td>"+nom_etab(etablissement[0])+"</td><td class='centrer'>"+str(etablissement[1])+" / "+etablissement[2]+"</td><td class='centrer'>"+etablissement[3]+" / "+etablissement[4]+"</td></tr>\n")
        fichier.write("</table></body></html>")
        fichier.close()

#Ecris toutes les données d'une ressource dans une chaine de caractère
def html_ressource_string(ark,debut,fin,img):
    #charge les cumuls par établissements correspondants à la ressource
    donnees_etablissements=[]
    with open('./data/bilan_'+ark+'.csv', encoding='utf-8', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='"')
        for row in reader:
            row[1]=int(row[1])
            donnees_etablissements.append(row)
        csvfile.close()

    #Trie par odre décroissant de connexions élèves
    donnees_etablissements=(sorted(donnees_etablissements, key=lambda x: x[1], reverse=True))

    #Créée la chaine de caractère
    html="<div style = 'display:block; clear:both; page-break-after:always;'></div><div class='titre' id='"+ark+"'><div class='home'><a href='#liste'>Retour 🏠</a></div><h1>Statistiques d'utilisation du GAR en Isère<br>Ressource "+str(nom_ressource(ark,False))+"</h1>\n"
    html+="<h3>Période du "+debut+" au "+fin+"<br>"+"</h3>\n</div>"
    html+="<h2>Nombre de connexions sur l\'année</h2>\n"
    html+=img.getvalue()
    html+="<h2>Établissements qui utilisent cette ressource</h2>\n<p style='font-size: x-small;'>Par ordre décroissant de connexion élèves<br>connexions cumulées sur l'année, ne représente pas le nombre d'utilisateurs distincts</p><table><tr class='descripteurs'><th>Nom de l'établissements</th><th>connexions<br>élè / ens</th><th>Licences<br>élè / ens</th></tr>"
    for etablissement in donnees_etablissements:
        html+="<tr><td>"+nom_etab(etablissement[0])+"</td><td class='centrer'>"+str(etablissement[1])+" / "+etablissement[2]+"</td><td class='centrer'>"+etablissement[3]+" / "+etablissement[4]+"</td></tr>\n"
    html+="</table>\n"
    return(html)

def pdf_ressource(ark):
    working_directory=os.path.abspath(os.getcwd())
    options = {
    'enable-local-file-access': True,
    'page-size': 'A4',
    'margin-top': '0in',
    'margin-right': '0in',
    'margin-bottom': '0in',
    'margin-left': '0in',
    'encoding': "UTF-8"
    }
    #css=os.path.abspath(os.getcwd())+'/style.css'
    #config = pdfkit.configuration(wkhtmltopdf='wkhtmltopdf/bin/wkhtmltopdf.exe')
    #pdfkit.from_file('./exports/'+ark+'.html', str(nom_ressource(ark))+'.pdf', options=options, verbose=True)
    os.system(r"wkhtmltopdf --margin-top 0 --margin-left 0 --margin-bottom 10 --margin-right 0 --encoding 'utf-8' --enable-local-file-access ./exports/"+ark+".html ./exports/"+str(nom_ressource(ark,True))+".pdf")

def html_bilan_ressource(data,debut,fin,html):
    #Trie les données dans l'ordre décroissant de la deuxième valeur
    data=(sorted(data, key=lambda x: x[1], reverse=True))

    #Crée le html
    with open('./exports/bilan_ressources.html', "w", encoding='utf-8') as fichier:
        fichier.write("<!doctype html>\n<html>\n<head>\n<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>\n<link href='"+os.path.abspath(os.getcwd())+"/style.css\' rel=\"stylesheet\" type=\"text/css\" />\n<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\"><link href=\"https://fonts.googleapis.com/css2?family=Acme&family=Open+Sans&family=Roboto&display=swap\" rel=\"stylesheet\">\n<body>\n")
        fichier.write("<div id='liste' class='titre new-page'><h1>Statistiques d'utilisation du GAR en Isère<br>Bilan des ressources</h1>\n")
        fichier.write("<h3>Période du "+debut+" au "+fin+"<br>"+"</h3>\n</div>")
        fichier.write("<h2>Liste des ressources</h2>\n<p style='font-size: x-small;'>Par ordre décroissant de connexion élèves<br>connexions cumulées sur l'année, ne représente pas le nombre d'utilisateurs distincts</p><table><tr class='descripteurs'><th>Nom de la ressource</th><th>connexions<br>élè / ens</th><th>Licences<br>élè / ens</th></tr>")
        for ressource in data:
            fichier.write("<tr><td><a href='#"+ressource[0]+"'>"+str(nom_ressource(ressource[0],False))+"</a></td><td class='centrer'>"+str(ressource[1])+" / "+str(ressource[2])+"</td><td class='centrer'>"+str(ressource[3])+" / "+str(ressource[4])+"</td></tr>\n")
        fichier.write("</table>")
        fichier.write(html)
        fichier.write("</body></html>")
        fichier.close()

def pdf_bilan():
    options = {
    'enable-local-file-access': True,
    'page-size': 'A4',
    'margin-top': '0in',
    'margin-right': '0in',
    'margin-bottom': '0in',
    'margin-left': '0in',
    'encoding': "UTF-8"
    }
    config = pdfkit.configuration(wkhtmltopdf='./wkhtmltopdf/bin/wkhtmltopdf.exe')
    pdfkit.from_file('./exports/bilan_ressources.html', './exports/bilan_ressources.pdf', options=options, verbose=True, configuration=config)
    #os.system(r"wkhtmltopdf --margin-top 0 --margin-left 0 --margin-bottom 10 --margin-right 0 --encoding 'utf-8' --enable-local-file-access ./exports/bilan_ressources.html ./exports/bilan_ressources.pdf")