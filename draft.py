# coding: utf-8
import csv
import os
from abonnements import *
from affichage import *
from donnees import *

#Nom des fichiers sources:
#./stats/Ressources_tne_dev.csv
#./stats/annuaire_etab.csv
# ./stats/Data_gar.csv

# Traite les listes d'abonnements par ressources, place les données dans le dossier data
files=os.listdir('./abonnements')
for file in files:
    abonnements_sans_doublon(file)
    
#Charge les ressources à traiter
Ressources=load_ressources_list('./stats/Ressources_tne.csv')

# Charge les donnees de connexions
Historique_connexion=load_connexions_data('./stats/Data_gar.csv')

# Calcul le nombre de jours depuis la rentrée
Dates=Historique_connexion[0][7:len(Historique_connexion[0])-8]
temps=len(Dates)

Tableau_recapitulatif_ressources=[] #[ark,nb de connexion ele, nb de connexion ens, nb de licences ele, nb de licences ens]
html_ressources_string=''

for ressource in Ressources :
    print(nom_ressource(ressource[0],True))
    # filtre le Historique_connexion par les donnees de la seule ressource
    connexions=extraction_donnees_ressource(Historique_connexion,ressource[0])
    
    #Calcule le nombre de connexions cumulées par jours
    connexions_cumulees=connexions_ressource_cumul(ressource[0],connexions,temps)

    #Calcule le nombre de connexions totale sur l'année par profil à la ressource
    connexions_total=connexions_ressource_total(ressource[0],connexions)
    
    #construit l'image (ressource_connexion.png) du graph des connexions dans le dossier data
    graph_connexions_ressources(ressource[0],connexions_cumulees,temps)
    




    


