# coding: utf-8
import csv

def abonnements_sans_doublon(filename):
    # Dans la liste des abonnements d'une ressource, cumul tous les abonnements d'un même établissement
    Abonnements=[]
    with open('./abonnements/'+filename, encoding='utf-8', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='"')
        next(reader)
        get_ark=False
        for row in reader:
            doublon=False
            if row[16]=='':
                row[16]=0
            if row[17]=='':
                row[17]=0
            if get_ark==False:
                ark=row[4]
                ark=ark.replace(":", "")
                ark=ark.replace("/", "")
            for i in range(len(Abonnements)):
                if Abonnements[i][0]==row[11] :
                    Abonnements[i][1]+=int(row[16])
                    Abonnements[i][2]+=int(row[17])
                    doublon=True
            if doublon==False :
                if row[17]=='NON' :
                    print(row)
                Abonnements.append([row[11],int(row[16]),int(row[17])])          
        Abonnements.pop(0) 
        csvfile.close()
    
    with open('./data/abo/abonnements_'+ark+'.csv','w', newline='') as csvfile :
        spamwriter = csv.writer(csvfile, delimiter=';')
        for row in Abonnements:
            spamwriter.writerow(row)
        csvfile.close()
            
def licences_ressource_par_etab(ark,uai):
    #extrait le nombre de licence par établissement pour une ressource
    #try open sinon print (manque telle ressourece)
    with open('./data/abo/abonnements_'+ark+'.csv', encoding='utf-8', newline='') as csvfile:
        licences=[0,0]
        reader = csv.reader(csvfile, delimiter=';', quotechar='"')
        for row in reader:
            if row[0]==uai:
                licences[0]+=int(row[1])
                licences[1]+=int(row[2])                    
        csvfile.close()
    return(licences)


def licences_ressource_total(ark):
    #calcul le nombre de licence global d'une ressource
    with open('./data/abo/abonnements_'+ark+'.csv', encoding='utf-8', newline='') as csvfile:
        licences=[0,0]
        reader = csv.reader(csvfile, delimiter=';', quotechar='"')
        for row in reader:
                licences[0]+=int(row[1])
                licences[1]+=int(row[2])                    
        csvfile.close()
    return(licences)
    
