#https://python-graph-gallery.com/313-bubble-map-with-folium/
import math
import csv
import folium
from io import StringIO
from affichage import *

def carte_connexions_globales(data):
    #charge la carte des établissements
    map = folium.Map(location=[45.4,5.5], tiles="OpenStreetMap", zoom_start=10)
    # add marker one by one on the map, and account for Mercator deformation lat=3 long=4
    for etab in data:
        local_deformation = math.cos(etab[3] * math.pi / 180)
        if etab[5] > 0:
            folium.Circle(
                location=[etab[3], etab[4]],
                popup='%s (%.1f)' % (nom_etab(etab[0]), int(etab[5])),
                radius=etab[5] * 0.1 * local_deformation,
                color='crimson',
                fill=True,
                fill_color='crimson'
            ).add_to(map)
    f = StringIO()
    map.save('carte.html')
    #map.save(f)
    #return(f)

def connexions_totales_uai(uai,filename):
    connexions=[0,0]
    for donnees in data:
        if donnees[2]==uai:
            if donnees[4]=='[Eleve]':
                connexions[0]+=int(donnees[-1])
            if donnees[4]=='[Enseignant]':
                connexions[1]+=int(donnees[-1])
    return(connexions)

def connexions_totales(data,filename):
    #charge la liste des établissements
    etablissements=[]
    with open(filename, encoding='utf-8', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='|')
        for row in reader:
            etablissements.append([row[0],row[1],row[2],float(row[3]),float(row[4]),0])
        csvfile.close()
    
    #ajoute le nombre de connexion élèves et le nombre de connexions profs
    for i in range (len(etablissements)):
        for etab in data:
            if etablissements[i][0]==etab[0]:
                #print(etab[1],etab[2])
                etablissements[i][5]+=int(etab[2])
    return(etablissements)


connexions_etab=[]
with open('./stats/Data_gar.csv', encoding='utf-16', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=';', quotechar='|')
    for row in reader:
        connexions_etab.append([row[2],row[4],row[-1]])
    csvfile.close()
avant_carte=connexions_totales(connexions_etab,'./stats/38-2D-CARTE.csv')

carte_connexions_globales(avant_carte)

options = {'javascript-delay': 50000,
    'page-size': 'Letter',
    'margin-top': '0.0in',
    'margin-right': '0.0in',
    'margin-bottom': '0.0in',
    'margin-left': '0.0in',
    'encoding': "UTF-8",
    'custom-header': [
        ('Accept-Encoding', 'gzip')
    ]}
pdfkit.from_file('carte.html',  'carte.pdf', options=options)
