# coding: utf-8
import csv
import matplotlib.pyplot as plt
from abonnements import *
from io import StringIO

def load_ressources_list(filename):
    data=[]
    with open(filename, encoding='utf-8', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='|')
        for row in reader:
            row[0]=x = row[0].replace(":", "")
            row[0]=x = row[0].replace("/", "")
            data.append(row)
        csvfile.close()
    return(data)
    
    
# Charge les connexions
def load_connexions_data(filename):
    data=[]
    with open(filename, encoding='utf-16', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='|')
        for row in reader:
            row[3]=x = row[3].replace(":", "")
            row[3]=x = row[3].replace("/", "")
            row2=[]
            for word in row:
                if word=='N/A':
                    word='0'
                row2.append(word)
            data.append(row2)
        csvfile.close()
    return(data)

def load_affectation_data(filename):
    # [2]= numéro de la ressource
    # Si [1] commence par 01, alors [-1]= nombre de licences affectées
    # Si [1] commence par 02, alors [-1]= nombre de licences supprimées
    
    with open(filename, encoding='utf-16', newline='') as csvfile:
        data=[]
        reader = csv.reader(csvfile, delimiter=';', quotechar='|')
        for row in reader:
            row[3]=x = row[3].replace(":", "")
            row[3]=x = row[3].replace("/", "")
            row2=[]
            for word in row:
                if word=='N/A':
                    word='0'
                row2.append(word)
            data.append(row2)
        csvfile.close()
    return(data)

def extraction_donnees_ressource(data,ressource):
# extrait des données globales, les donnés pour la seule ressource
    data_ressource=[]
    for donnees in data:
        if ressource == donnees[3]:
            data_ressource.append(donnees)    
    return (data_ressource)

def connexions_ressource_cumul(ressource,data,duree):
# cumul les connexions élèves et enseignants des différents établissements pour une même ressource
    data_eleves=[0 for n in range(duree)]
    data_enseignants=[0 for n in range(duree)]
    for donnees in data:
        if donnees[4]=='[Eleve]':
            for i in range(duree):
                data_eleves[i]+=int(donnees[i+7])
        if donnees[4]=='[Enseignant]':
            for i in range(duree):
                data_enseignants[i]+=int(donnees[i+7])
    return([data_eleves,data_enseignants])

def connexions_ressource_total(ark,data):
    #calcul le nombre de connexions global d'une ressource
    # récupère le tableau de données et vérifie l'ark de la ligne, si c'est bon et que c'est élève , on ajoute la dernière valeur de la ligne à connexions[0] si c'est prof ....)
    connexions=[0,0]
    for donnees in data:
        if donnees[4]=='[Eleve]':
           connexions[0]+=int(donnees[-1])
        if donnees[4]=='[Enseignant]':
            connexions[1]+=int(donnees[-1])
    return(connexions)

def connexions_et_licences_ressource_etab(ressource,data):
# liste les connexions élèves et enseignants des différents établissements pour cette ressource
    data_etablissements=[]
    for donnees in data:
        exist=False
        for i in range(len(data_etablissements)):
            #on regarde si l'établissement est déjà dans la liste
            if data_etablissements[i][0]==donnees[2]:
                exist=True
                #on met à jour ses données
                if donnees[4]=='[Eleve]':
                    data_etablissements[i][1]=int(donnees[-7])
                if donnees[4]=='[Enseignant]':
                    data_etablissements[i][2]=int(donnees[-7])
        #sinon on ajoute un nouvel établissement dans la liste en y indiquant également son nombre de licence
        if exist==False:
            licences=licences_ressource_par_etab(ressource,donnees[2])
            if donnees[4]=='[Eleve]':
                    data_etablissements.append([donnees[2],int(donnees[-7]),0,licences[0],licences[1]])
            if donnees[4]=='[Enseignant]':
                    data_etablissements.append([donnees[2],0,int(donnees[-7]),licences[0],licences[1]])
    return(data_etablissements)


def graph_connexions_ressources(ressource,data,duree):
#crée un graph représentant le nombre de connexions élèves et enseignants
#creation du graphique
    plt.figure(figsize=(30, 10), dpi=40)
    jours=[n for n in range(duree)]
    plt.plot(jours,data[0], label="sessions élèves")
    plt.plot(jours,data[1], label="sessions enseignants")
    plt.legend(loc="upper left", frameon=False, fontsize="xx-large")
    plt.xticks(rotation = 70)
    plt.yticks(fontsize="xx-large")
    plt.gca().tick_params(axis = 'x', length = 0)
    plt.tick_params(axis = 'x', length = 0)
    #plt.savefig('data/img/'+ressource+"_connexions.svg", bbox_inches = "tight", pad_inches=0)
    f = StringIO()
    plt.savefig(f, format='svg', bbox_inches = "tight", pad_inches=0)
    plt.close()
    return(f)
                
def affectees_ressource_cumul(ressource):
    #todo
    return()

def bilan_ressource(donnees,ark):
        with open('./data/bilan_'+ark+'.csv','w', newline='') as csvfile :
            spamwriter = csv.writer(csvfile, delimiter=';')
            for row in donnees:
                spamwriter.writerow(row)
        csvfile.close()