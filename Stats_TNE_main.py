# coding: utf-8
import csv
import os
from abonnements import *
from affichage import *
from donnees import *

#Nom des fichiers sources:
#./stats/Ressources_tne_dev.csv
#./stats/annuaire_etab.csv
# ./stats/Data_gar.csv

# Traite les listes d'abonnements par ressources, place les données dans le dossier data
files=os.listdir('./abonnements')
for file in files:
    abonnements_sans_doublon(file)
    
#Charge les ressources à traiter
Ressources=load_ressources_list('./stats/Ressources_tne.csv')

# Charge les donnees de connexions
Historique_connexion=load_connexions_data('./stats/Data_gar.csv')

# Calcul le nombre de jours depuis la rentrée
Dates=Historique_connexion[0][7:len(Historique_connexion[0])-8]
temps=len(Dates)

Tableau_recapitulatif_ressources=[] #[ark,nb de connexion ele, nb de connexion ens, nb de licences ele, nb de licences ens]
html_ressources_string=''

for ressource in Ressources :
    print(nom_ressource(ressource[0],True))
    # filtre le Historique_connexion par les donnees de la seule ressource
    connexions=extraction_donnees_ressource(Historique_connexion,ressource[0])
    
    #Calcule le nombre de connexions cumulées par jours
    connexions_cumulees=connexions_ressource_cumul(ressource[0],connexions,temps)

    #Calcule le nombre de connexions totale sur l'année par profil à la ressource
    connexions_total=connexions_ressource_total(ressource[0],connexions)
    
    #construit l'image (ressource_connexion.png) du graph des connexions dans le dossier data
    img_ressource=graph_connexions_ressources(ressource[0],connexions_cumulees,temps)
    
    #Construit la liste des établissements utilisant la ressource [uai,nb de connexion ele, nb de connexion ens, nb de licences ele, nb de licences ens]
    liste_connexions_et_licences=connexions_et_licences_ressource_etab(ressource[0],connexions)

    # Donnees globales
    #Calcul le nombre global de licences obtenues ([ele,ens]) et l'ajoute aux données)
    licences_total=licences_ressource_total(ressource[0])    
    
    #Calcul le nombre global de licences attribuées ([ele,ens]) et l'ajoute aux données)
    #affectees_total=affectees_ressource_cumul(ressource[0])    
    
    #Enregistre le bilan de la ressource
    Tableau_recapitulatif_ressources.append([ressource[0],connexions_total[0],connexions_total[1],licences_total[0],licences_total[1]])

    with open('./data/bilan_'+ressource[0]+'.csv','w', newline='') as csvfile :
        spamwriter = csv.writer(csvfile, delimiter=';')
        for row in liste_connexions_et_licences:
            spamwriter.writerow(row)
        csvfile.close()

    #Construction du fichier html
    #html_ressource(ressource[0],Dates[0],Dates[-1])

    #Ajout en mise en forme html
    html_ressources_string+=html_ressource_string(ressource[0],Dates[0],Dates[-1],img_ressource)


    #conversion du html en pdf
    #pdf_ressource(ressource[0])


html_bilan_ressource(Tableau_recapitulatif_ressources,Dates[0],Dates[-1],html_ressources_string)

pdf_bilan()
   




    


